# Assessment

# Custom Product Export Profile

### 1. About Task

Create a new export profile to export products with attribute option label text instead of attribute option code by following existing export steps. New export should not reflect any change in core product export.

#### 1.1 Install

Copy and paste the custom bundle code in to `src/Acme/Bundle/`
Once place the folder then run the below commands.

#### 1.2 Install Commands.

```
bin/console cache:clear --env=dev
sudo rm -rf var/cache/

sudo php bin/console assets:install --env=dev
sudo php bin/console assets:install --env=dev --symlink
sudo php bin/console --env=dev pim:installer:assets --symlink --clean

yarn run webpack-dev
```

