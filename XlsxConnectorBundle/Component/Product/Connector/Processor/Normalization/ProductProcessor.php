<?php

namespace Acme\Bundle\XlsxConnectorBundle\Component\Product\Connector\Processor\Normalization;

use Akeneo\Pim\Enrichment\Component\Product\Model\EntityWithFamilyInterface;
use Akeneo\Pim\Enrichment\Component\Product\Model\ProductModelInterface;
use Akeneo\Pim\Enrichment\Component\Product\ValuesFiller\EntityWithFamilyValuesFillerInterface;
use Akeneo\Pim\Structure\Component\Repository\AttributeRepositoryInterface;
use Akeneo\Tool\Component\Batch\Item\DataInvalidItem;
use Akeneo\Tool\Component\Batch\Item\ItemProcessorInterface;
use Akeneo\Tool\Component\Batch\Job\JobInterface;
use Akeneo\Tool\Component\Batch\Job\JobParameters;
use Akeneo\Tool\Component\Batch\Model\StepExecution;
use Akeneo\Tool\Component\Batch\Step\StepExecutionAwareInterface;
use Akeneo\Tool\Component\Connector\Processor\BulkMediaFetcher;
use Akeneo\Tool\Component\StorageUtils\Repository\IdentifiableObjectRepositoryInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Akeneo\Pim\Structure\Component\AttributeTypes;
use Akeneo\Pim\Enrichment\Component\Product\Value\OptionValue;
use Akeneo\Pim\Enrichment\Component\Product\Value\OptionsValue;
use Akeneo\Pim\Enrichment\Component\Product\Connector\Processor\Normalization\ProductProcessor as BaseProcessor;

/**
 * Product processor to process and normalize entities to the standard format
 *
 * @author    Syed <syed.bakrudin@ziffity.com>
 * 
 */
class ProductProcessor extends BaseProcessor
{
    /** @var NormalizerInterface */
    protected $normalizer;

    /** @var IdentifiableObjectRepositoryInterface */
    protected $channelRepository;

    /** @var AttributeRepositoryInterface */
    protected $attributeRepository;

    /** @var StepExecution */
    protected $stepExecution;

    /** @var BulkMediaFetcher */
    protected $mediaFetcher;

    /** @var EntityWithFamilyValuesFillerInterface */
    protected $productValuesFiller;

    /** @var IdentifiableObjectRepositoryInterface|null */
    private $attributeOptionRepository;

    /**
     * @param NormalizerInterface                   $normalizer
     * @param IdentifiableObjectRepositoryInterface $channelRepository
     * @param AttributeRepositoryInterface          $attributeRepository
     * @param BulkMediaFetcher                      $mediaFetcher
     * @param EntityWithFamilyValuesFillerInterface $productValuesFiller
     */
    public function __construct(
        NormalizerInterface $normalizer,
        IdentifiableObjectRepositoryInterface $channelRepository,
        AttributeRepositoryInterface $attributeRepository,
        ?IdentifiableObjectRepositoryInterface $attributeOptionRepository = null,
        BulkMediaFetcher $mediaFetcher,
        ?EntityWithFamilyValuesFillerInterface $productValuesFiller = null
    ) {
        $this->normalizer          = $normalizer;
        $this->channelRepository   = $channelRepository;
        $this->attributeRepository = $attributeRepository;
        $this->attributeOptionRepository = $attributeOptionRepository;
        $this->mediaFetcher        = $mediaFetcher;
        $this->productValuesFiller = $productValuesFiller;
    }

    /**
     * {@inheritdoc}
     */
    public function process($product)
    {
        $parameters = $this->stepExecution->getJobParameters();
        $structure = $parameters->get('filters')['structure'];
        $channel = $this->channelRepository->findOneByIdentifier($structure['scope']);
        if ($product instanceof ProductModelInterface) {
            $this->productValuesFiller->fillMissingValues($product);
        }

        $productStandard = $this->normalizer->normalize(
            $product,
            'standard',
            [
                'filter_types' => ['pim.transform.product_value.structured'],
                'channels' => [$channel->getCode()],
                'locales'  => array_intersect(
                    $channel->getLocaleCodes(),
                    $parameters->get('filters')['structure']['locales']
                ),
            ]
        );

        if ($this->areAttributesToFilter($parameters)) {
            $attributesToFilter = $this->getAttributesToFilter($parameters);
            $productStandard['values'] = $this->filterValues($productStandard['values'], $attributesToFilter);
        }

        if ($parameters->has('with_media') && $parameters->get('with_media')) {
            $directory = $this->stepExecution->getJobExecution()->getExecutionContext()
                ->get(JobInterface::WORKING_DIRECTORY_PARAMETER);

            $this->fetchMedia($product, $directory);
        } else {
            $mediaAttributes = $this->attributeRepository->findMediaAttributeCodes();
            $productStandard['values'] = array_filter(
                $productStandard['values'],
                function ($attributeCode) use ($mediaAttributes) {
                    return !in_array($attributeCode, $mediaAttributes);
                },
                ARRAY_FILTER_USE_KEY
            );
        }
        
        /**
         * Data process for display simple select option code into lable in Export profile.
         */
        $options = [];
        $scopeCode = $structure['scope'];
        $localeCode = $structure['locales'][0];

        /* Get the lable based on code by Locale */
        foreach ($this->getAttributeCodes($product) as $attributeCode) {
            $attribute = $this->attributeRepository->findOneByIdentifier($attributeCode);
            $locale = $attribute->isLocalizable() ? $localeCode : null;
            $scope = $attribute->isScopable() ? $scopeCode : null;

            /* Simple Select Option value Process */
            if (null !== $attribute && AttributeTypes::OPTION_SIMPLE_SELECT === $attribute->getType()) {
                $optionValue = $product->getValue($attributeCode, $locale, $scope);
                if ($optionValue instanceof OptionValue) {
                    $optionCode = $optionValue->getData();
                    $option = $this->attributeOptionRepository->findOneByIdentifier($attributeCode . '.' . $optionCode);                 
                    $option->setLocale($localeCode);
                    $translation = $option->getTranslation();
                    $options[$attributeCode]['value'] = null !== $translation->getValue() ? $translation->getValue() : sprintf('[%s]', $option->getCode());
                }
            }

            /* Multi Select Option value Process */
            if (null !== $attribute && AttributeTypes::OPTION_MULTI_SELECT === $attribute->getType()) {
                $optionValue = $product->getValue($attributeCode, $locale, $scope);
                if ($optionValue instanceof OptionsValue) {
                    $optionCodes = $optionValue->getData();
                    $labels = [];
                    foreach ($optionCodes as $optionCode) {
                        $option = $this->attributeOptionRepository->findOneByIdentifier($attributeCode.'.'.$optionCode);
                        $option->setLocale($localeCode);
                        $translation = $option->getTranslation();
                        $labels[] = null !== $translation->getValue() ? $translation->getValue() : sprintf('[%s]', $option->getCode());
                    }
                    $options[$attributeCode]['value'] = $labels;
                }
            }
        }

        /* Replace code by lable */
        if(count($options)>0) {
            foreach($options as $key => $codeValue) {
                if (array_key_exists($key,$productStandard['values'])) {
                    $productStandard['values'][$key][0]['data'] = $codeValue['value'];
                }
            }
        }
        
        /* End */

        return $productStandard;
    }

    /**
     * {@inheritdoc}
     */
    public function setStepExecution(StepExecution $stepExecution)
    {
        $this->stepExecution = $stepExecution;
    }

    /**
     * Fetch medias on the local filesystem
     *
     * @param EntityWithFamilyInterface $product
     * @param string           $directory
     */
    protected function fetchMedia(EntityWithFamilyInterface $product, $directory)
    {
        $identifier = $product instanceof ProductModelInterface ? $product->getCode() : $product->getIdentifier();
        $this->mediaFetcher->fetchAll($product->getValues(), $directory, $identifier);

        foreach ($this->mediaFetcher->getErrors() as $error) {
            $this->stepExecution->addWarning($error['message'], [], new DataInvalidItem($error['media']));
        }
    }

    /**
     * Filters the attributes that have to be exported based on a product and a list of attributes
     *
     * @param array $values
     * @param array $attributesToFilter
     *
     * @return array
     */
    protected function filterValues(array $values, array $attributesToFilter)
    {
        $valuesToExport = [];
        $attributesToFilter = array_flip($attributesToFilter);
        foreach ($values as $code => $value) {
            if (isset($attributesToFilter[$code])) {
                $valuesToExport[$code] = $value;
            }
        }

        return $valuesToExport;
    }

    /**
     * Return a list of attributes to export
     *
     * @param JobParameters $parameters
     *
     * @return array
     */
    protected function getAttributesToFilter(JobParameters $parameters)
    {
        $attributes = $parameters->get('filters')['structure']['attributes'];
        $identifierCode = $this->attributeRepository->getIdentifierCode();
        if (!in_array($identifierCode, $attributes)) {
            $attributes[] = $identifierCode;
        }

        return $attributes;
    }

    /**
     * Are there attributes to filter?
     *
     * @param JobParameters $parameters
     *
     * @return bool
     */
    protected function areAttributesToFilter(JobParameters $parameters)
    {
        return isset($parameters->get('filters')['structure']['attributes'])
            && !empty($parameters->get('filters')['structure']['attributes']);
    }

    protected function getAttributeCodes($product)
    {
        return $product->getUsedAttributeCodes();
    }
}
