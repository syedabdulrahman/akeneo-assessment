<?php

/*
 * This file is part of the Symfony package.
 * @purpose: Product export in XLSX format with option by lable
 *
 * (c) Syed <syed.bakrudin@ziffity.com>
 * 
 * file that was distributed with this source code.
 */

namespace Acme\Bundle\XlsxConnectorBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Bundle.
 */

class AcmeXlsxConnectorBundle extends Bundle
{
}